/**
 * @author 蔡治平
 * @since 2015-08-13
 */
Ext.define('Demo.view.NewsPanel', {
    extend: 'Ext.List',
    xtype: "newspanel",
    requires: ['Demo.store.News'],
    config: {
    	itemTpl:  new Ext.XTemplate(
		    '<div class="news">'
			+'<div><img class="x-img" src="{img}"/></div>'
			+'<div style="title">{title}<p class="content">{content}</p></div>'
			+'<div style="clear:both"></div>'
			+'</div>'
		),
    	store: Ext.create('Demo.store.News')
    }
});