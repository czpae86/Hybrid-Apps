/**
 * @author 蔡治平
 * @since 2015-08-13
 */
Ext.define('Demo.model.News', {
    extend: 'Ext.data.Model',
    config: {
        fields : ['id','title','createtime','img','content']
    }
});