/**
 * @author 蔡治平
 * @since 2015-08-12
 */


function initApp() {
    document.addEventListener("deviceready", onDeviceReady, false);
}

function onBackKeyDown(){
	Ext.Msg.confirm("温馨提示", "是否退出程序?", function(buttonId){
		if(buttonId=='yes')
		navigator.app.exitApp(); 
	});
}

function onDeviceReady(){
	
	document.addEventListener("backbutton", onBackKeyDown, false);
	
	Ext.Loader.setConfig({
		enabled : true
	});
	
	Ext.application({
	    name: 'Demo',
	    controllers: ['Main'],
	    views: ['Main'],    
	    launch: function() {
			Ext.create('Demo.view.Main');
	        var menu = Ext.create("Ext.Menu", {
	            defaults: {
	                xtype: "button"
	            },
	            width: '70%',
	            scrollable: 'vertical',
	            items: [
	                {
	                    text: '我的计划',
	                    iconCls: 'time'
	                },
	                {
	                    text: '我的位置',
	                    iconCls: 'locate'
	                },
	                {
	                    text: '查询',
	                    iconCls: 'search'
	                },
	                {
	                    text: '设置',
	                    iconCls: 'settings'
	                },
	                {
	                    text: '个人信息',
	                    iconCls: 'info'
	                }
	            ]
	        });
	
	        Ext.Viewport.setMenu(menu, {
	            side: 'left',
	            cover : false,
	            reveal: false
	        });
	    }
	});
}
