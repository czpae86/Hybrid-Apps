/**
 * @author 蔡治平
 * @since 2015-08-13
 */
Ext.define('Demo.store.News', {
    extend: 'Ext.data.Store',
    requires: ['Demo.model.News'],
    config :{
        model: 'Demo.model.News',
        data: [
	       { title: 'Tommy',   content: 'Maintz'  },
	       { title: 'Rob',     content: 'Dougan'  },
	       { title: 'Ed',      content: 'Spencer' },
	       { title: 'Jamie',   content: 'Avins'   },
	       { title: 'Aaron',   content: 'Conran'  },
	       { title: 'Dave',    content: 'Kaneda'  },
	       { title: 'Jacky',   content: 'Nguyen'  },
	       { title: 'Abraham', content: 'Elias'   },
	       { title: 'Jay',     content: 'Robinson'},
	       { title: 'Nigel',   content: 'White'   },
	       { title: 'Don',     content: 'Griffin' },
	       { title: 'Nico',    content: 'Ferrero' },
	       { title: 'Jason',   content: 'Johnston'}
	   ]
    }
});