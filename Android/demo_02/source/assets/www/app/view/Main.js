/**
 * @author 蔡治平
 * @since 2015-08-12
 */
Ext.define('Demo.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: "mainpanel",
    requires: ['Demo.view.NewsPanel'],
    config: {
    	fullscreen: true,
        tabBarPosition: 'bottom',
	    items: [
	        {
	            xtype : 'titlebar',
	            docked: 'top',
	            title: '统一信息平台',
	            items : [{
	            	xtype : 'button',
	            	align : 'left',
	            	iconCls : 'list'
	            },{
	            	xtype : 'button',
	            	align : 'right',
	            	iconCls : 'search'
	            }]
	        },{
	            title: '资讯',
	            iconCls: 'home',
	            layout : 'fit',
	            items : [{
	            	xtype : 'newspanel'
	            }]
	        },
	        {
	            title: '待办',
	            iconCls: 'bookmarks',
	            html: '我的待办'
	        },
	        {
	            title: '联系人',
	            iconCls: 'user',
	            html: 'Contact Screen'
	        },
	        {
	            title: '收藏夹',
	            iconCls: 'star',
	            html: '收藏夹'
	        }
	    ]
    }
});