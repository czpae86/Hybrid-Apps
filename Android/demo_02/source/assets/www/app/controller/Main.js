/**
 * @author 蔡治平
 * @since 2015-08-12
 */
Ext.define('Demo.controller.Main', {
    extend: 'Ext.app.Controller',
    config: {
        control: {
            'mainpanel button[iconCls=list]' : {
            	tap : 'onMenuBtnClick'
            }
        },
        refs: {
        	mainPanel : 'mainpanel'
        }
    },
    onMenuBtnClick : function(){
    	Ext.Viewport.toggleMenu("left");
    }
});