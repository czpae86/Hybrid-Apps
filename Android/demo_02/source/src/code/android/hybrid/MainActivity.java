package code.android.hybrid;

import org.apache.cordova.Config;
import org.apache.cordova.CordovaActivity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
/**
 * 
 * @author 蔡治平
 * @since 2015-08-12
 *
 */
public class MainActivity extends CordovaActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.init();
		// Load your application
		super.loadUrl(Config.getStartUrl());
	}

}
